#define L_MOTOR 8   
#define L_MOTOR_PWM 9
#define R_MOTOR_PWM 10 
#define R_MOTOR 11

void setup_motors();

void forward(int speed);

void back(int speed);

void left(int speed);

void right(int speed);

void turn_left(int speed);

void turn_right(int speed);

void stop();
