#include "motors.h"

void setup_motors() {
   // Подготавливаем моторы
  pinMode(L_MOTOR,OUTPUT);
  pinMode(R_MOTOR,OUTPUT); 
  pinMode(L_MOTOR_PWM,OUTPUT); 
  pinMode(R_MOTOR_PWM,OUTPUT); 
}

void forward(int speed)    
{
  // Устанавливаем скорость моторов
  analogWrite(R_MOTOR_PWM, speed); 
  analogWrite(L_MOTOR_PWM, speed);
    
  // Включаем моторы вперёд
  digitalWrite(R_MOTOR, LOW); 
  digitalWrite(L_MOTOR, LOW);
}

void back(int speed)
{
  // Устанавливаем скорость моторов
  analogWrite(R_MOTOR_PWM, speed); 
  analogWrite(L_MOTOR_PWM, speed);
    
  // Включаем моторы назад
  digitalWrite(R_MOTOR, HIGH); 
  digitalWrite(L_MOTOR, HIGH);

}

void left(int speed)         
{
  // Устанавливаем скорость моторов
  analogWrite(R_MOTOR_PWM, speed); 
  analogWrite(L_MOTOR_PWM, 0);
    
  // Включаем только правые моторы
  digitalWrite(R_MOTOR, LOW); 
  digitalWrite(L_MOTOR, LOW);
}

void right(int speed)        
{
  // Устанавливаем скорость моторов
  analogWrite(R_MOTOR_PWM, 0); 
  analogWrite(L_MOTOR_PWM, speed);
    
  // Включаем только левые моторы
  digitalWrite(R_MOTOR, HIGH); 
  digitalWrite(L_MOTOR, LOW);
}

void turn_left(int speed)         
{
  // Устанавливаем скорость моторов
  analogWrite(R_MOTOR_PWM, speed); 
  analogWrite(L_MOTOR_PWM, speed);
    
  // Включаем моторы в разные стороны
  digitalWrite(R_MOTOR, LOW); 
  digitalWrite(L_MOTOR, HIGH);
}

void turn_right(int speed)        
{
  // Устанавливаем скорость моторов
  analogWrite(R_MOTOR_PWM, speed); 
  analogWrite(L_MOTOR_PWM, speed);
    
  // Включаем моторы в разные стороны
  digitalWrite(R_MOTOR, HIGH); 
  digitalWrite(L_MOTOR, LOW);
}

void stop()
{
  // Выключаем моторы
  analogWrite(R_MOTOR_PWM, LOW); 
  analogWrite(L_MOTOR_PWM, LOW); 
}
