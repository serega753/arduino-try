#include "servo_distance.h"

void setup_servo_distance(Servo* servo, int angle) {
  servo->attach(ULTRASONIC_BASE_PIN);
  servo->write(angle);
}

void set_servo_distance_angle(Servo* servo, int angle) {
  servo->write(angle);
  delay(20);
}
