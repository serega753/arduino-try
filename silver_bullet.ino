#include <Servo.h>

#include "motors.h"
#include "ic_sensors.h"
#include "distance.h"
#include "beep.h"
#include "servo_distance.h"

// Минимальная дистанция до препятствия перед объездом
#define MIN_DISTANCE 20

// Сообщение с Bluetooth
char msg = '\0'; 

//// Переменная скорости
int speed = 150;

Servo ultrasonic_base;
int angle = 90;

void setup() { 
  // Открываем соединение с Serial портом
  Serial.begin(9600);
  setup_ic_sensors();
  setup_motors();
  setup_distance();
  setup_servo_distance(&ultrasonic_base, angle);
}

void escape_wall() {
  turn_left(200);
  delay(300);
  stop();
}


 
void loop() {
  float distance = get_distance();

  // Избегание препятствий путём объезда
  if(distance < MIN_DISTANCE) {
    escape_wall();
  }

  // Избегание столкновений с препятствиями путём остановки

  // Проверка на препятствие впереди 
  if (is_wall_ahead() && msg == 'F' || is_wall_behind() && msg == 'B') {
    // Пищим о препятствии впереди
    beep();
    stop();
  }

  // Если пришли данные с Bluetooth
  if (Serial.available()) {
    // Считываем сообщение
    msg = Serial.read();
    Serial.println(msg);
    // Едем вперёд, если нет препятствий
    switch(msg) {
        case 'F':
          if (!is_wall_ahead()) {
            forward(speed);
          }
        case 'B':
          if (!is_wall_behind()) {
            back(speed);
          }
        case 'L':
           left(speed);
        case 'R':
           right(speed);
        case 'S':
           stop();
        default:
          if (msg >= '0' && msg <= '9') {
          // Переводим сообщение в угол сервомотора
            angle = (msg - '0') * 20;
            set_servo_distance_angle(&ultrasonic_base, angle);
          }
      }
  }
}
