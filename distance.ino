#include "distance.h"


void setup_distance() {
    // Подготавливаем дальномер
  pinMode(TRIG, OUTPUT);  
  pinMode(ECHO, INPUT); 
}

float get_distance() {
  digitalWrite(TRIG, LOW);
  delayMicroseconds(2);
  
  digitalWrite(TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG, LOW);

  return pulseIn(ECHO, HIGH)*0.017;
}
