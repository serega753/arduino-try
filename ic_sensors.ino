#include "ic_sensors.h"

void setup_ic_sensors() {
    // Подготавливаем ИК датчики
  pinMode(FRONT_OBST, INPUT);
  pinMode(BACK_OBST, INPUT);
}

bool is_wall_ahead() {
  return !digitalRead(FRONT_OBST);
}

bool is_wall_behind() {
  return !digitalRead(BACK_OBST);
}

bool is_wall() {
  return is_wall_behind() || is_wall_ahead();
}
